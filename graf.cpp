
#include "graf.h"

using namespace std;

// Dodanie elementu do listy. wierz. grafu. 
int Graf::dodaj_element(int i, int j, int wartosc) // wierz. pocz / wierz. konc / waga 
{
	element = new Element_listy;
	element->wezel = j;
	element->waga = wartosc;
	element->nastepny = ListaGrafu[i];
	ListaGrafu[i] = element;
	return MacierzGrafu[i][j] = wartosc;
}


void Graf::wyswietl_macierz() // WY�WIETLANIE MACIERZY
{
	for (int i = 0; i < ilosc_wierz; i++)
	{
		for (int j = 0; j < ilosc_wierz; j++)
		{
			cout << MacierzGrafu[i][j] << "\t";
		}
		cout << endl;
	}
}

void Graf::wyswietl_liste() // WY�WIETLENIE LISTY
{
	Element_listy* pomocniczy;
	for (int i = 0; i < ilosc_wierz; i++)
	{
		cout << "L[" << i << "] =";
		pomocniczy = ListaGrafu[i]; // Wypisanie wagi z wierzch i-tego do wierzcho�k�w z listy s�siedztwa 
		while (pomocniczy)
		{
			cout << pomocniczy->wezel << "(" << pomocniczy->waga << ") ";
			pomocniczy = pomocniczy->nastepny; // bierze nast�pny element listy 
		}
		cout << endl;
	}
}

Graf::Graf(int wielkosc)
{
	ilosc_wierz = wielkosc;
	MacierzGrafu = new int* [ilosc_wierz];

	for (int i = 0; i < ilosc_wierz; i++)
		MacierzGrafu[i] = new int[ilosc_wierz];
	///////////////////////////////////////////////////////
	for (int i = 0; i < ilosc_wierz; i++)
		for (int j = 0; j < ilosc_wierz; j++)
			MacierzGrafu[i][j] = 0;
	//////////////////////////////////////////////////////////////
	ListaGrafu = new Element_listy * [ilosc_wierz];

	for (int i = 0; i < ilosc_wierz; i++)
		ListaGrafu[i] = NULL;



}


// RELAKSACJA WIERZCHOLKOW, zwraca true i false. Jeszcze zwraca true to znaczy, ze juz wszystkie drogi zostaly znalezione. False - wystapil ujemny cykl. 
bool Graf::BF_L(int start)
{
	int i, x;
	bool test; // Czy algorytm wprowadzi� zmiany - true nie wprowadzi� - false wprowadzi�
	Element_listy* pv; // tworzymy obiekt struktury Element_listy 

	d[start] = 0; // Zeruje koszty doj�cia
	for (i = 1; i < ilosc_wierz; i++) // P�TLA RELAKSACJI 
	{
		test = true; // Oznacza to, �e algorytm nie wprowadzi� zmian do p i d 
		for (x = 0; x < ilosc_wierz; x++) // Przechodz� przez kolejne wierzch. grafu
			for (pv = ListaGrafu[x]; pv; pv = pv->nastepny) // Przegl�dam list� s�siad�w wierzch.
				                                            // Je�eli obceny koszt drogi do wierzcho�ka x (d[x]) jest wi�kszy ni� droga z wierzch. start.  do wierzcho�ka x przez 
				                                           // wierzch s�siaduj�cy - czyli droga do wierzch. s�siaduj�cego + droga mi�dzy nimi to zmieniamy ta drog�  
			
				if (d[pv->wezel] > d[x] + pv->waga) // sprawdzam warunek relaksacji 
				{
					test = false; // jest zmiana w d i p 
					d[pv->wezel] = d[x] + pv->waga; // relaksuje kraw�d� z x do jego s�siada
					p[pv->wezel] = x; // poprzednikiem s�siada b�dzie x 
				}
		if (test) return true;  // Je�li nie by�o zmian, to ko�czymy
	}

	// SPRAWDZENIE CZY ISTNIEJE UJEMNY CYKL - Gdy wykonamy wszystkie relaksacje i wykonamy jedn� dodatkow� p�tle 
	// i znaleziono jeszcze jak�� kr�tsz� drog� to cykl ujemny 
	for (x = 0; x < ilosc_wierz; x++)
		for (pv = ListaGrafu[x]; pv; pv = pv->nastepny)
			if (d[pv->wezel] > d[x] + pv->waga) return false; // UJEMNY CYKL 
				

	return true;
}


bool Graf::BF_M(int start)
{
	int i, x;
	bool test;

	d[start] = 0; // Zeruje koszty doj�cia
	for (i = 1; i < ilosc_wierz; i++) // P�TLA RELAKSACJI 
	{
		test = true; // oznacza to,  �e algorytm nie wprowadzi� zmian do p i d
		for (x = 0; x < ilosc_wierz; x++) // przechodzi przez kolejne wierzcho�ki grafu // kolumny 
			for (int j = 0; j < ilosc_wierz; j++) // sprawdza wiersze po kolei 
				if (d[j] > d[x] + MacierzGrafu[x][j] && MacierzGrafu[x][j] != 0) // je�li obecnie zapisany koszt do wierzcho�ka j(wiersz) jest wi�kszy ni� droga do wierzcho�ka x
					                                                            // + droga mi�dzy wierzcho�kami j i x to zapisujemy, �e zasz�a zmiana (test=false) w p i d 
					                                                            // i przypisujemy now�, kr�tsz� drog� d[x] + MacierzGrafu[x][j]. Do p[x] wpisujemy przez jaki 
					                                                            // wierzcho�ek przeszli�my. 
				{
					test = false;
					d[j] = d[x] + MacierzGrafu[x][j];
					p[j] = x;
				}
		if (test) return true; // Je�li nie by�o zmiany to ko�czymy 
	}


	// SPRAWDZENIE CZY ISTNIEJE UJEMNY CYKL - Gdy wykonamy wszystkie relaksacje i wykonamy jedn� dodatkow� p�tle i znaleziono jeszcze jak�� kr�tsz� drog� to cykl ujemny 
	for (x = 0; x < ilosc_wierz; x++)
		for (int j = 0; j < ilosc_wierz; j++)
			if (d[j] > d[x] + MacierzGrafu[x][j] && MacierzGrafu[x][j] != 0) return false;

	return true; // nie zasz�a zmiana 
}

// Utworzenie tablicy d i p, tablice p ustawia na -1, d na +niesk. Potem wywo�ujemy funkcj� BF_L, kt�ra robi (liczbawierzch-1)  * relaksacj�. Dzi�ki temu 
// otrzymujemy najkr�tsze drogi do danych wierzch. 
void Graf::BellmanaForda_Lista() {

	int i, x, sptr, * S;
	bool czy_koniec;
	d = new long long[ilosc_wierz]; // tablica koszt�w 
	p = new int[ilosc_wierz]; // tablica "poprzednik�w" 
	for (i = 0; i < ilosc_wierz; i++)
	{
		d[i] = INT_MAX;
		p[i] = -1; // ustawienie tablicy p[i] na -1, 
	}

	czy_koniec = BF_L(start); // je�li znajdzie wszystkie najkr�tsze �cie�ki to zwraca true, je�li nie to false
	if (czy_koniec)  // je�li znaleziono ju� wszystkie najkr�tsze �cie�ki to: 
	{
		S = new int[ilosc_wierz]; // Pami�� mo�na zarezerwowa� po to, by zapisa� tam jakie� dane, za pomoc� operatora new
		sptr = 0;


		ofstream plik;
		plik.open("WynikiLista"); // ZAPIS DO PLIKU WynikiLista
		for (i = 0; i < ilosc_wierz; i++)
		{
			plik << i << ": "; // wypisanie numeru wierzcho�ka 
			for (x = i; x != -1; x = p[x]) // wierzcho�ki �cie�ki umieszczami na stosie
				S[sptr++] = x; // w kolejno�ci od ostatniego do pierwszego

			while (sptr) // wierzcho�ki ze stosu dopisujemy do pliku (czyli  kolejne wierzch, przez jakie przechodzi) 
				plik << S[--sptr] << " "; // w kolejno�ci od pierwszego do ostatniego

			plik << "Koszt drogi dla tego wierzcho�ka to: " << d[i] << endl; // wypisanie kosztu 
		}

		delete[] S; // USUNI�CIE STOSU 
	}
	else cout << "ujemna sciezka" << endl;
}


void Graf::BellmanaForda_Macierz() {

	int i, x, sptr, * S;
	bool czy_koniec;
	d = new long long[ilosc_wierz];// tablica koszt�w 
	p = new int[ilosc_wierz]; // tablica "poprzednikow" 
	for (i = 0; i < ilosc_wierz; i++) 
	{
		d[i] = INT_MAX; // ustawienie koszt�w na nieskonczonosc
		p[i] = -1; // ustawienie dr�g na -1
	}
	cout << endl;
	czy_koniec = BF_M(start); // je�li znajdzie wszystkie najkr�tsze �cie�ki to zwraca true, je�li nie to false

	if (czy_koniec) // je�li znaleziono ju� wszystkie najkr�tsze �cie�ki to: 
	{
		S = new int[ilosc_wierz]; // Pami�� mo�na zarezerwowa� po to, by zapisa� tam jakie� dane za pomoc� operatora new 
		sptr = 0;


		ofstream plik;
		plik.open("WynikiTab");// ZAPIS DO PLIKU WynikiLista
		for (i = 0; i < ilosc_wierz; i++) 
		{
			plik << i << ": "; // wypisanie numeru wierzcho�ka
			for (x = i; x != -1; x = p[x]) // wierzcho�ki �cie�ki umieszczami na stosie
				S[sptr++] = x; // w kolejno�ci od ostatniego do pierwszego

			while (sptr) // wierzcho�ki ze stosu dopisujemy do pliku (czyli kolejne wierzcho�ki przez jakie przechodzi)
				plik << S[--sptr] << " "; // w kolejno�ci od pierwszego do ostatniego

			plik << "Koszt drogi dla tego wierzcho�ka to: " << d[i] << endl; // wypisanie kosztu
		}

		delete[] S; // USUNI�CIE STOSU
	}
	else cout << "ujemna sciezka" << endl;
}


Graf::~Graf() // DESTRUKTUR 
{
	for (int i = 0; i < ilosc_wierz; i++)
		delete[] MacierzGrafu[i];
	delete[] MacierzGrafu;

	delete[] ListaGrafu;
}




