﻿
#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include "graf.h"

using namespace std;

///////////////////////////////////////////

Graf* Wczytaj(Graf* nowy)
{
	int ilosc_krawedzi;
	int ilosc_wierz;
	int wierz_start;
	int wierz_poccz;
	int wierz_kon;
	int waga;
	string nazwa_pliku;
	ifstream plik;
	cout << "Wpisz nazwe pliku, ktory chcesz wczytac:"<< endl;
	cin >> nazwa_pliku;

	plik.open(nazwa_pliku.c_str()); // otworzenie wpisanego pliku, ktory wczesniej generuje się 

	if (plik.good())
	{
		plik >> ilosc_krawedzi; // wczytanie ilości krawędzi
		plik >> ilosc_wierz; // wczytanie ilości wierzchołków
		plik >> wierz_start; // wczytanie wierzchołka startowego 
		nowy = new Graf(ilosc_wierz); // wskaźnik nowy wskazuje teraz na nowo utworzony obiekt klasy graf o danej ilości wierzch. 
		nowy->startowy(wierz_start); // teraz przypisuje argumentowi startowy obiektu graf wierz_startowy. 
		while (!plik.eof())
		{ // pobieranie kolejnych wierszy z pliku
			plik >> wierz_poccz;
			plik >> wierz_kon;
			plik >> waga;
			// dodanie pobranych elementów 
			nowy->dodaj_element(wierz_poccz, wierz_kon, waga);
		}

	}
	else
	{
		cout << "zla nazwa pliku" << endl;
	}
	plik.close();
	return nowy;

}

// Generacja grafu i utworzenie pliku o wybranej nazwie. 
void generuj()
{
	int ilosc_krawedzi;
	int ilosc_wierz;
	int wierz_start;

	string nazwa_pliku;
	ofstream plik;
	int waga;
	double gestosc;
	cout << "Podaj nazwe pliku, ktory chcesz wygenerowac:" << endl;
	cin >> nazwa_pliku;

	plik.open(nazwa_pliku.c_str()); //  c_str() konwertuje ciąg znaków zapisany w zmiennej typu string na ciąg 
	//                                który może być zapisany w tablicy znaków. Odwołujemy się do niej za pomocą operatora ".".
	cout << "Podaj liczbe wierzocholkow, a po spacji wiercholek startowy:" << endl;
	cin >> ilosc_wierz >> wierz_start;
	cout << "Podaj gestosc grafu w procentach [%]" << endl;
	cin >> gestosc;
	ilosc_krawedzi = (ilosc_wierz * (ilosc_wierz - 1)) / 2;
	ilosc_krawedzi = ilosc_krawedzi * gestosc /100;
	if (plik.good())
	{

		plik << ilosc_krawedzi << " " << ilosc_wierz << " " << wierz_start << endl; // PIERWSZY WIERSZ - ilość krawędzi/ilość wierzchołków/wierzch. start. 
		
		// KOLEJNE WIERSZE - wierzchołek początkowy/wierzchołek końcowy/waga
		for (int i = 0; i < ilosc_wierz; i++)
			for (int j = i; j < ilosc_wierz; j++)
				if (i != j)
				{
					waga = rand() % 50 + 1; // waga jest losowana do 50 
					plik << i << " " << j << " " << waga << endl; 
					plik << j << " " << i << " " << waga << endl;
				}
	}
	else
	{
		cout << "zla nazwa pliku" << endl;
	}
	plik.close();

}


void menu()
{
	ofstream plik; // obsługujemy wyjście pliku (ofstream)
	plik.open("Wynikiczas", ios::app); // utworzenie pliku tekstowego do ktorego zapisze czasy wykonywania algorytmu 
	unsigned int opcja;
	int ilosc_wierz = 1;
	Graf* Nowy = NULL; // utworzenie wskaznika na graf 
	int* Tab_drogi = new int[ilosc_wierz]; // Tablica dla "kosztu" drogi miedzy dwoma wierzocholkami  
	while (1)
	{
		cout << " 0 - Zamknij program |" << endl;
		cout << " 1 - Wyswietl dane | ";
		cout << " 2 - Generuj graf i wybierz gestosc |" << endl;
		cout << " 3 - Wczytaj graf z pliku |" ;
		cout << " 4 - Wykonaj algorytm BellmanaForda (lista sasiedztwa) dla wczytanego grafu. |" << endl ;
		cout << " 5 - Wykonaj algorytm BellmanaForda (macierz sasiedztwa) dla wczytanego grafu. |";
		
		cout << endl<< " Wybierz:" << endl;
		cin >> opcja;

		switch (opcja)
		{
		case 0: return;
		case 1:
			if (Nowy == NULL)
				cout << "Brak macierzy i listy " << endl;
			else
			{
				Nowy->wyswietl_macierz(); // wyświetla macierz

				cout << endl << endl;
				Nowy->wyswietl_liste(); // wyświetla liste 
			}

			break;
		case 2:
			generuj();
			break;
		case 3:
			Nowy = Wczytaj(Nowy);
			break;
		case 4:
		{
			clock_t start = clock();
			Nowy->BellmanaForda_Lista();
			plik << "Czas dla listy: " << clock() - start << endl;
		}
		break;
		case 5:
		{
			clock_t start = clock();
			Nowy->BellmanaForda_Macierz();
			plik << "Czas dla macierzy: " << clock() - start << endl;
		}
		break;
		default:
			cout << "Brak takiej opcji" << endl;
			break;
		}
	}
}

int main()
{

	menu();
}
