
#pragma once
#include <iostream>
#include <fstream>

using namespace std;

struct Element_listy {
	Element_listy* nastepny;
	int wezel;
	int waga;
};

class Graf
{
public:
	Graf(int wielkosc = 1);
	~Graf();
	int dodaj_element(int i, int j, int wartosc);
	void wyswietl_macierz();
	void wyswietl_liste();

	void BellmanaForda_Lista();
	bool BF_L(int start);

	void BellmanaForda_Macierz();
	bool BF_M(int start);

	// przypisanie wierzch. start. do obiektu 
	void startowy(int start) { this->start = start; }


private:
	long long* d; // koszty 
	int* p; // kolejne "przystanki" drogi
	Element_listy* element; // do tworzenia kolejnych element�w na li�cie 
	Element_listy** ListaGrafu; // Do listy grafu 
	int** MacierzGrafu; // do macierzy grafu 
	int start; // wierzcho�ek startowy 
	int ilosc_wierz;
	int ilosc_krawedzi;
	
};

